package ocow.com.sscs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;

import org.w3c.dom.Text;

/**
 * Created by dlark on 03/10/2017.
 */

public class TinderAdapter extends ArrayAdapter<TinderClass> {
    public TinderAdapter (Context context,int resource){
        super(context, resource);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        SelectableRoundedImageView iv = (SelectableRoundedImageView) convertView.findViewById(R.id.tinder_image);
        TextView tv1 = (TextView) convertView.findViewById(R.id.tinder_title);
        TextView tv2 = (TextView) convertView.findViewById(R.id.tinder_sub_title);

        iv.setImageResource(getItem(position).getImage());
        tv1.setText(getItem(position).getTitle());
        tv2.setText(getItem(position).getSubTitle());

        return convertView;
    }

}
