package ocow.com.sscs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaCodec;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Pattern;

import static android.R.attr.button;

//public class MainActivity extends AppCompatActivity implements View.OnClickListener{
public class MainActivity extends AppCompatActivity {
    private Button buttonRegister;
    private EditText editEmail, editPass, editForename, editSurname;
    private TextView login;

    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    JSONObject data = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getJSON("Dundalk,Ireland");

        Intent in = new Intent(this, LoginActivity.class);
        startActivity(in);

//        progressDialog = new ProgressDialog(this);
//
//        firebaseAuth = FirebaseAuth.getInstance();
//
//        if(firebaseAuth.getCurrentUser()!= null){
//            finish();
//            Intent in = new Intent(this, HomeActivity.class);
//            startActivity(in);
//        }
//
//        databaseReference = FirebaseDatabase.getInstance().getReference();
//
//        buttonRegister = (Button) findViewById(R.id.btn_signup);
//        editEmail = (EditText) findViewById(R.id.editText);
//        editPass = (EditText) findViewById(R.id.passEditText);
//        editForename = (EditText) findViewById(R.id.forename);
//        editSurname = (EditText) findViewById(R.id.surname);
//        login = (TextView) findViewById(R.id.login);
//
//        buttonRegister.setOnClickListener(this);
//        login.setOnClickListener(this);
    }

    public void getJSON(final String city){
        Log.d("test", "this is a test");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params){
                Log.d("test", "This is a test");
                try {
                    URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=" + city +
                            "&AppID=7f54b1d35d34e682ffc5c8f27853cb45");

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    StringBuffer json = new StringBuffer(1024);
                    String tmp = "";

                    while((tmp = reader.readLine()) != null) {
                        json.append(tmp).append("\n");
                        reader.close();
                    }

                    data = new JSONObject(json.toString());

                    if(data.getInt("cod") != 200) {
                        System.out.println("Cancelled");
                        return null;
                    }
                } catch (Exception e) {
                    System.out.println("Exception " + e.getMessage());
                    return null;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void Void) {
                if(data != null) {
                    Log.d("Weather received", data.toString());
                }
            }
        }.execute();
    }

// private void saveUserInfo() {
//        String forename = editForename.getText().toString().trim();
//        String surname = editSurname.getText().toString().trim();
//
//        UserInformation userInformation = new UserInformation(forename, surname);
//        FirebaseUser user = firebaseAuth.getCurrentUser();
//        databaseReference.child("users").child(user.getUid()).setValue(userInformation);
//    }
//
//    private void registerUser() {
//        String email = editEmail.getText().toString().trim();
//        String password = editPass.getText().toString().trim();
//        String forename = editForename.getText().toString().trim();
//        String surname = editSurname.getText().toString().trim();
//
//        if(TextUtils.isEmpty(forename)){
//            Toast.makeText(this, "Please enter forename", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        if(TextUtils.isEmpty(surname)){
//            Toast.makeText(this, "Please enter surname", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        if(TextUtils.isEmpty(email)){
//            //email is empty
//            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if(TextUtils.isEmpty(password)) {
//            //Password is empty
//            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        if(!isVaildPassword(password)){
//            Toast.makeText(this, "Please enter a longer password", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//
////        if(password.length() < 6){
////            Toast.makeText(this, "Password too short", Toast.LENGTH_LONG).show();
////            return;
////        }
//
//        progressDialog.setMessage("Signing up...");
//        progressDialog.show();
//
//        firebaseAuth.createUserWithEmailAndPassword(email, password)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if(task.isSuccessful()){
//                            saveUserInfo();
//                            finish();
//                            Intent in = new Intent(getApplicationContext(), HomeActivity.class);
//                            startActivity(in);
//                        } else {
//                            Toast.makeText(MainActivity.this, "Registered Failed, try again.", Toast.LENGTH_SHORT).show();
//                            return;
//                        }
//                    }
//                });
//    }
//
//    public static boolean isVaildPassword(String s) {
//        Pattern PASSWORD_PATTERN = Pattern.compile("[a-zA-Z0-9\\!\\@\\#]{6,24}");
//        return !TextUtils.isEmpty(s) && PASSWORD_PATTERN.matcher(s).matches();
//    }
//
//    @Override
//    public void onClick(View view) {
//        if(view == buttonRegister){
//            registerUser();
//        }
//        if(view == login) {
//            finish();
//            Intent in = new Intent(MainActivity.this, LoginActivity.class);
//            startActivity(in);
//        }
//    }
}
