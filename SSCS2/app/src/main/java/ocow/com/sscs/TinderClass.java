package ocow.com.sscs;

/**
 * Created by dlark on 03/10/2017.
 */

public class TinderClass {
    int image;
    String title;
    String subTitle;

    public TinderClass(int img, String str1, String str2) {
        this.image = img;
        this.title = str1;
        this.subTitle = str2;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
}
