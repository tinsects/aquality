package ocow.com.sscs;

import android.content.Intent;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import static android.R.attr.keycode;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView user_email, user_name;
    private Button logout_btn;

    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;

    private DatabaseReference databaseReference;
    private DatabaseReference databaseRefereneceForename;
    private DatabaseReference databaseRefereneceSurname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        firebaseUser = firebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(firebaseUser.getUid());
        Log.i("UID",firebaseUser.getUid());
        databaseRefereneceSurname = databaseReference.child("surname");
        databaseRefereneceForename = databaseReference.child("forename");
        Log.i("dRForename",databaseRefereneceForename.toString());
        Log.i("dRSurname",databaseRefereneceSurname.toString());

        user_email = (TextView) findViewById(R.id.pro_email_tv);
        user_name = (TextView) findViewById(R.id.pro_name_tv);
        logout_btn = (Button) findViewById(R.id.logout_btn);

        user_email.setText(firebaseUser.getEmail());

        databaseRefereneceForename.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("forename", dataSnapshot.getValue().toString());
                user_name.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        databaseRefereneceSurname.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("surname", dataSnapshot.getValue().toString());
                user_name.append(" " + dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        logout_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == logout_btn){
            firebaseAuth.signOut();
            finish();
            Intent in = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(in);
        }
    }

    private void updateUserInfo() {
//        String forename = editForename.getText().toString().trim();
//        String surname = editSurname.getText().toString().trim();

//        UserInformation userInformation = new UserInformation(forename, surname);
//        FirebaseUser user = firebaseAuth.getCurrentUser();
//        databaseReference.child(user.getUid()).setValue(userInformation);
    }


}
