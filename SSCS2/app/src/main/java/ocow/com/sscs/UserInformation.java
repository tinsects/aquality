package ocow.com.sscs;

/**
 * Created by dlark on 27/09/2017.
 */

public class UserInformation {

    public String forename;
    public String surname;

    public UserInformation(){

    }

    public UserInformation(String forename, String surname) {
        this.forename = forename;
        this.surname = surname;
    }
}
