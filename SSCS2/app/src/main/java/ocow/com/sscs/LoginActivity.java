package ocow.com.sscs;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText email_et, pass_et;
    private Button login_btn;
    private TextView signUp_tv;

    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() != null) {
            finish();
            Intent in = new Intent(this, HomeActivity.class);
            startActivity(in);
        }

        email_et = (EditText) findViewById(R.id.editText);
        pass_et = (EditText) findViewById(R.id.passEditText);
        login_btn = (Button) findViewById(R.id.btn_login);
        signUp_tv = (TextView) findViewById(R.id.sign_tv);

        progressDialog = new ProgressDialog(this);

        login_btn.setOnClickListener(this);
        signUp_tv.setOnClickListener(this);
    }

    private void userLogin() {
        String email = email_et.getText().toString().trim();
        String pass = pass_et.getText().toString().trim();

        if(TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter email!", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(pass)){
            Toast.makeText(this, "Please enter pasword!", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Logging in...");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if(task.isSuccessful()){
                            finish();
                            Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(in);
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        if(view == login_btn){
            userLogin();
        }

        if(view == signUp_tv) {
            finish();
            Intent in = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(in);
        }
    }
}
