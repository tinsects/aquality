package ocow.com.sscs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageButton profile_btn;
    private RelativeLayout g1_btn, g2_btn, g3_btn, g4_btn, g5_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        profile_btn = (ImageButton) findViewById(R.id.profile_btn);
        g1_btn = (RelativeLayout) findViewById(R.id.group1_btn);
        g2_btn = (RelativeLayout) findViewById(R.id.group2_btn);
        g3_btn = (RelativeLayout) findViewById(R.id.group3_btn);
        g4_btn = (RelativeLayout) findViewById(R.id.group4_btn);
        g5_btn = (RelativeLayout) findViewById(R.id.group5_btn);

        profile_btn.setOnClickListener(this);
        g1_btn.setOnClickListener(this);
        g2_btn.setOnClickListener(this);
        g3_btn.setOnClickListener(this);
        g4_btn.setOnClickListener(this);
        g5_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == profile_btn) {
            Intent in = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(in);
        }
        if(view == g1_btn){
            Intent in = new Intent(getApplicationContext(), TinderActivity.class);
            in.putExtra("group", 1);
            startActivity(in);
        }
        if(view == g2_btn){
            Intent in = new Intent(getApplicationContext(), TinderActivity.class);
            in.putExtra("group", 2);
            startActivity(in);
        }
        if(view == g3_btn){
            Intent in = new Intent(getApplicationContext(), TinderActivity.class);
            in.putExtra("group", 3);
            startActivity(in);
        }
        if(view == g4_btn){
            Intent in = new Intent(getApplicationContext(), TinderActivity.class);
            in.putExtra("group", 4);
            startActivity(in);
        }
        if(view == g5_btn){
            Intent in = new Intent(getApplicationContext(), TinderActivity.class);
            in.putExtra("group", 5);
            startActivity(in);
        }
    }
}
