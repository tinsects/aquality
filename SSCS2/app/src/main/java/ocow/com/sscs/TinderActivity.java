package ocow.com.sscs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wenchao.cardstack.CardStack;

import java.util.ArrayList;

public class TinderActivity extends AppCompatActivity implements View.OnClickListener, CardStack.CardEventListener {

    Bundle bundle;
    int group;
    private ImageButton profile_btn, no_btn, yes_btn;
    private LinearLayout linearLayout_card;
    private CardStack cardStack;
    private TinderAdapter tinderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tinder);

        bundle = getIntent().getExtras();
        group = bundle.getInt("group");

        profile_btn = (ImageButton) findViewById(R.id.profile_btn);
        no_btn = (ImageButton) findViewById(R.id.no_btn);
        yes_btn = (ImageButton) findViewById(R.id.yes_btn);
        linearLayout_card = (LinearLayout) findViewById(R.id.card_linear);

        if(group == 1){
            group1();
        } else if(group == 2) {
            group2();
        } else if(group == 3) {
            group3();
        } else if(group == 4) {
            group4();
        } else {
            group5();
        }

        initImages();
        cardStack = (CardStack) findViewById(R.id.cardStack);
        cardStack.setContentResource(R.layout.tinder_card);
        cardStack.setStackMargin(20);
        cardStack.setAdapter(tinderAdapter);
        cardStack.setListener(this);

        profile_btn.setOnClickListener(this);
        no_btn.setOnClickListener(this);
        yes_btn.setOnClickListener(this);
    }

    private void initImages() {
        tinderAdapter = new TinderAdapter(getApplicationContext(),0);
        tinderAdapter.add(new TinderClass(R.drawable.ecdyonurus,"Edyonurus","Palse or Late March Brown, Green Dun"));
        tinderAdapter.add(new TinderClass(R.drawable.rhithrogena_semicolorata1,"Rhithrogrna","Olive Upright"));
        tinderAdapter.add(new TinderClass(R.drawable.heptagenia,"Heptagenia","Yellow May Dun"));
        tinderAdapter.add(new TinderClass(R.drawable.ephemera_danica,"Ephemera Danica","The MayFly"));
        tinderAdapter.add(new TinderClass(R.drawable.serratella_ignita,"Serratella Ignita","Blue Winged Olive, Yellow Evening Dun"));
        tinderAdapter.add(new TinderClass(R.drawable.caenis,"Caenis","Angler's Curse"));
    }

    @Override
    public void onClick(View view) {
        if(view == profile_btn) {
            Intent in = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(in);
        }
        if(view == no_btn){

        }
        if(view == yes_btn){

        }
    }

    public void group1() {
        Toast.makeText(getApplicationContext(), "Group 1",Toast.LENGTH_SHORT).show();
        linearLayout_card.setBackgroundResource(R.drawable.group1_btn);
    }

    public void group2(){
        Toast.makeText(getApplicationContext(), "Group 2",Toast.LENGTH_SHORT).show();
        linearLayout_card.setBackgroundResource(R.drawable.group2_color_radius);
    }

    public void group3(){
        Toast.makeText(getApplicationContext(), "Group 3",Toast.LENGTH_SHORT).show();
        linearLayout_card.setBackgroundResource(R.drawable.group3_color_radius);
    }

    public void group4(){
        Toast.makeText(getApplicationContext(), "Group 4",Toast.LENGTH_SHORT).show();
        linearLayout_card.setBackgroundResource(R.drawable.group4_color_radius);
    }

    public void group5(){
        Toast.makeText(getApplicationContext(), "Group 5",Toast.LENGTH_SHORT).show();
        linearLayout_card.setBackgroundResource(R.drawable.group5_color_radius);
    }

    @Override
    public boolean swipeEnd(int direction, float distance) {
        return (distance > 200)?true:false;
    }

    @Override
    public boolean swipeStart(int direction, float distance) {
        return true;
    }

    @Override
    public boolean swipeContinue(int direction, float distanceX, float distanceY) {
        return true;
    }

    @Override
    public void discarded(int id, int direction) {
        if(direction == 1 || direction == 3){
            Toast.makeText(getApplicationContext(), "Added",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Discarded",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void topCardTapped() {
        Toast.makeText(getApplicationContext(), "Top card pressed",Toast.LENGTH_SHORT).show();
    }


}
